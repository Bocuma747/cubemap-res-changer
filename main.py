import bspmodule
import shutil
import io
import os
import struct
import threading
from tkinter import Tk, filedialog, ttk, font, messagebox, Grid, Toplevel, IntVar, StringVar

class Main:
    def __init__(self, master):

        def selectfiles():
            # Open file dialog
            bsppath = filedialog.askopenfilename(title="Select .bsp file(s)",
                                                 filetypes=[("BSP files (.bsp)", "*.bsp")],
                                                 multiple=True,
                                                 parent=master)
            # Add selected files to the treeview
            for filepath in bsppath:
                self.tree_FilesList.insert('', 'end', text=os.path.abspath(filepath))

        def removeselected():
            selection = self.tree_FilesList.selection()
            for i in selection[::-1]:
                self.tree_FilesList.delete(i)

        def callback_ComboboxSelected(event):
            effective_resolution = (2** (self.cbox_Resolution.current() - 1)) * 4
            # If the user's monitor is lower resolution than the resolution they selected * 4,
            # warn them that buildcubemaps won't work.
            if effective_resolution > self.screenwidth or effective_resolution > self.screenheight:
                messagebox.showwarning("Warning",
                                       "Your monitor's resolution must be at least %dx%d to use this setting. Otherwise, buildcubemaps will fail." % (effective_resolution, effective_resolution))

        def run():
            if len(self.tree_FilesList.get_children()) == 0:
                pass
            else:
                # Popup GUI
                self.newwindow = Toplevel(self.master)
                Grid.columnconfigure(self.newwindow, 0, weight=1)
                status = StringVar()
                self.statuslabel = ttk.Label(self.newwindow, textvariable=status)
                self.statuslabel.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")
                progressbar = ttk.Progressbar(self.newwindow, mode="indeterminate")
                progressbar.start(15)
                progressbar.grid(row=1, column=0, padx=10, pady=10, sticky="nsew")
                resolution = self.cbox_Resolution.current()

                # Iterate through the file list
                for item in self.tree_FilesList.get_children():
                    filepath = os.path.abspath(self.tree_FilesList.item(item)['text'])
                    bspname = os.path.splitext(os.path.basename(filepath))[0]

                    # Make a backup of the bsp if the user chose to
                    if self.var_makebackup.get() == 1:
                        status.set("%s - Backing up file" % os.path.basename(filepath))
                        shutil.copy(filepath, os.path.join(os.path.dirname(filepath), bspname+".bsp.backup"))


                    with open(filepath, "rb+") as bspfile:
                        status.set("%s - Reading cubemaps" % os.path.basename(filepath))
                        read_bsp = bspmodule.BSP(bspfile.read())
                        # Lump 42 is the cubemap lump in all BSP versions.
                        cubemap_lump_get = read_bsp.get_lump(42)
                        # Make a file-like object and write the original cubemap lump to it.
                        # This is what we'll read from.
                        cubemap_lump_original = io.BytesIO(cubemap_lump_get)
                        # Make an empty file-like object that we'll write the new lump to.
                        cubemap_lump_output = io.BytesIO()


                        # https://developer.valvesoftware.com/wiki/Source_BSP_File_Format#Cubemap
                        # Each dcubemapsample_t structure is 4 integers: the cubemap's coordinates (xyz)
                        # and the resolution.
                        # Since a dcubemapsample_t structure is 16 bytes, the number of structures is
                        # the length of the lump divided by 16.
                        status.set("%s - Writing cubemaps" % os.path.basename(filepath))
                        for i in range(len(cubemap_lump_get)//16):
                            origin = cubemap_lump_original.read(12)
                            cubemap_lump_output.write(origin + struct.pack('i', resolution))
                            cubemap_lump_original.read(4)           # Skip past the old resolution
                            cubemap_lump_output.seek(0, 2)          # Seek to end of new lump

                        bspfile.seek(0, 0)
                        # Everything before the cubemap lump
                        bsp_part1 = bspfile.read(read_bsp.get_lump_header(42)["offset"])
                        bspfile.read(read_bsp.get_lump_header(42)["length"])
                        # Everything after the cubemap lump
                        bsp_part2 = bspfile.read()

                        status.set("%s - Saving new bsp" % os.path.basename(filepath))
                        with open(filepath, "wb") as output:
                            output.write(bsp_part1 + cubemap_lump_output.getvalue() + bsp_part2)

                self.newwindow.destroy()

        def makethread(thread):
            self.thread = threading.Thread(target=thread)
            self.thread.start()

        # GUI
        self.master = master
        self.master.title("Cubemap resolution changer")
        self.style = ttk.Style()
        self.fontSegoe10 = font.Font(family="Segoe UI", size=10)
        self.style.configure("TButton", font=self.fontSegoe10)
        self.style.configure("TLabel", font=self.fontSegoe10)
        Grid.rowconfigure(self.master, 1, weight=1)
        Grid.columnconfigure(self.master, 0, weight=1)

        self.b_OpenFiles = ttk.Button(self.master,
                                      text="Open .bsp file(s)...",
                                      command=selectfiles)
        self.b_OpenFiles.grid(row=0, column=0, sticky="w", padx=10, pady=10)

        self.tree_FilesList = ttk.Treeview(self.master, show="tree")
        self.tree_FilesList.grid(row=1, column=0, sticky="nsew", padx=10, pady=10)

        self.b_RemoveSelected = ttk.Button(self.master,
                                           text="Remove selected",
                                           command=removeselected)
        self.b_RemoveSelected.grid(row=2, column=0, sticky="e", padx=10, pady=10)

        ttk.Separator(orient="horizontal").grid(row=3, column=0, sticky="nsew", pady=10)

        ttk.Label(self.master, text="Change the map's cubemap resolution to:").grid(row=4,
                                                                                    column=0,
                                                                                    sticky="ew",
                                                                                    padx=10,
                                                                                    pady=10)

        resolutions = ["0: Default (32x32)",
                       "1: 1x1",
                       "2: 2x2",
                       "3: 4x4",
                       "4: 8x8",
                       "5: 16x16",
                       "6: 32x32",
                       "7: 64x64",
                       "8: 128x128",
                       "9: 256x256",
                       "10: 512x512",
                       "11: 1024x1024",
                       "12: 2048x2048",
                       "13: 4096x4096"]

        self.cbox_Resolution = ttk.Combobox(self.master,
                                            values=resolutions,
                                            state="readonly",
                                            height=14)
        # Set default value
        self.cbox_Resolution.current(newindex=0)
        self.cbox_Resolution.bind('<<ComboboxSelected>>', callback_ComboboxSelected)
        self.cbox_Resolution.grid(row=5, column=0, padx=40, pady=10, sticky="ew")

        self.var_makebackup = IntVar()
        self.var_makebackup.set(1)
        self.checkbutton_Makebackup = ttk.Checkbutton(master,
                                                      text="Backup .bsp file(s)",
                                                      variable=self.var_makebackup)
        self.checkbutton_Makebackup.grid(row=6, column=0, padx=60, pady=5)

        self.b_Go = ttk.Button(self.master,
                               text="Go",
                               command=lambda: makethread(run))
        self.b_Go.grid(row=7, column=0, padx=10, pady=10)


        # Window geometry
        self.master.update()
        self.screenwidth = self.master.winfo_screenwidth()
        self.screenheight = self.master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth/3,
                                         self.screenheight/2)
        self.master.geometry(self.initwindowsize)


if __name__ == '__main__':
    root = Tk()
    RUN_GUI = Main(root)
    root.mainloop()
